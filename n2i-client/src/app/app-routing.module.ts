import { ModuleWithProviders } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: './templates/main/main.module#MainModule'
  },
  {
    path: 'authentication',
    loadChildren: './templates/authentication/authentication.module#AuthenticationModule'
  },
  {
    path: '**',
    redirectTo: '',
  }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes, {
  preloadingStrategy: PreloadAllModules,
  useHash: false,
  // enableTracing: true
});
