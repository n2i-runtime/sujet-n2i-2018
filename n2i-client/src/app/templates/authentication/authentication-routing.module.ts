import { RouterModule, Routes } from '@angular/router';

import { AuthenticationComponent } from './authentication.component';

import { AuthComponent } from '../../pages/auth/auth.component';


export const routes: Routes = [
  {
    path: '',
    component: AuthenticationComponent,
    children: [
      {
        path: '',
        component: AuthComponent
      },
    ]
  }
];

export const AuthenticationRoutingModule = RouterModule.forChild(routes);
