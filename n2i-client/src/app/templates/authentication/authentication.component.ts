import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})

export class AuthenticationComponent implements OnInit {
  private isAuthenticated = true;

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (this.isAuthenticated) {
      this.router.navigate(['']);
    }
  }
}
