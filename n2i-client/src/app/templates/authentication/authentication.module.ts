import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AuthenticationComponent } from './authentication.component';

import { AuthenticationRoutingModule } from './authentication-routing.module';

import { AuthModule } from '../../pages/auth/auth.module';

@NgModule({
  declarations: [
    AuthenticationComponent,
  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    AuthModule,
    MDBBootstrapModule.forRoot()
  ]
})

export class AuthenticationModule { }
