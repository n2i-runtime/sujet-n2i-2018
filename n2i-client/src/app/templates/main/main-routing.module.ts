import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './main.component';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import {NewCharacterComponent} from "../../pages/new-character/new-character.component";


export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'new-character',
        component: NewCharacterComponent
      }
    ]
  }
];

export const MainRoutingModule = RouterModule.forChild(routes);
