import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DropdownModule, NavbarModule} from 'angular-bootstrap-md';

import { MainComponent } from './main.component';

import { MainRoutingModule } from './main-routing.module';

import { DashboardModule } from '../../pages/dashboard/dashboard.module';
import {NewCharacterModule} from "../../pages/new-character/new-character.module";

@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    DashboardModule,
    NewCharacterModule,
    NavbarModule,
    DropdownModule
  ],
  providers: [],
  exports: []
})

export class MainModule { }
