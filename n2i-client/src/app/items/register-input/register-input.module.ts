import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ButtonsModule, CheckboxModule, IconsModule } from 'angular-bootstrap-md';

import { RegisterInputComponent } from './register-input.component';

@NgModule({
  declarations: [
    RegisterInputComponent
  ],
  imports: [
      CommonModule,
      CheckboxModule,
      IconsModule,
      ButtonsModule,
      FormsModule,
      ReactiveFormsModule
  ],
  exports: [
      RegisterInputComponent
  ],
  entryComponents: [
  ]
})

export class RegisterInputModule { }
