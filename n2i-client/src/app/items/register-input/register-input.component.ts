import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register-input',
  templateUrl: './register-input.component.html',
  styleUrls: ['./register-input.component.scss'],
})

export class RegisterInputComponent {
  @Output() onRegister = new EventEmitter();

  public userPasswordText: string;
  public registerForm = new FormGroup({
    email: new FormControl( ''),
    password: new FormControl(''),
  });


  constructor() { }

  public onSubmit() {
    this.onRegister.emit(this.registerForm.value);
  }
}
