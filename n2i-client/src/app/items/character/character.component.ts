import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})

export class CharacterComponent {
  @Output() onLogin = new EventEmitter();

  constructor() { }

  public onClickLogin() {
    this.onLogin.emit();
  }
}
