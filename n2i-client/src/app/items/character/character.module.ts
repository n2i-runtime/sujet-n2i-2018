import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CharacterComponent } from './character.component';
import {ButtonsModule, CheckboxModule, IconsModule} from "angular-bootstrap-md";

@NgModule({
  declarations: [
    CharacterComponent
  ],
  imports: [
      CommonModule,
      CheckboxModule,
      IconsModule,
      ButtonsModule
  ],
  exports: [
      CharacterComponent
  ]
})

export class CharacterModule { }
