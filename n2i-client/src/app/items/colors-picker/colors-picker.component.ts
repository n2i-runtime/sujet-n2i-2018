import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './colors-picker.component.html',
  styleUrls: ['./colors-picker.component.scss']
})

export class ColorsPickerComponent {
  @Output() onLogin = new EventEmitter();

  constructor() { }

  public onClickLogin() {
    this.onLogin.emit();
  }
}
