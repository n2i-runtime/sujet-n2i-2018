import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ColorsPickerComponent } from './colors-picker.component';
import {ButtonsModule, CheckboxModule, IconsModule} from "angular-bootstrap-md";

import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  declarations: [
    ColorsPickerComponent
  ],
  imports: [
      CommonModule,
      CheckboxModule,
      IconsModule,
      ButtonsModule,
      ColorPickerModule
  ],
  exports: [
      ColorsPickerComponent
  ]
})

export class ColorsPickerModule { }
