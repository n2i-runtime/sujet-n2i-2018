import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginInputComponent } from './login-input.component';
import {ButtonsModule, CheckboxModule, IconsModule} from "angular-bootstrap-md";

@NgModule({
  declarations: [
    LoginInputComponent
  ],
  imports: [
      CommonModule,
      CheckboxModule,
      IconsModule,
      ButtonsModule
  ],
  exports: [
      LoginInputComponent
  ]
})

export class LoginInputModule { }
