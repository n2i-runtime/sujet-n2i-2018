import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-login-input',
  templateUrl: './login-input.component.html',
  styleUrls: ['./login-input.component.scss']
})

export class LoginInputComponent {
  @Output() onLogin = new EventEmitter();

  constructor() { }

  public onClickLogin() {
    this.onLogin.emit();
  }
}
