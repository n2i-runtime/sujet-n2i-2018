import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthComponent } from './auth.component';

import { RegisterInputModule } from '../../items/register-input/register-input.module';
import { LoginInputModule } from '../../items/login-input/login-input.module';

import { RegisterInputComponent } from '../../items/register-input/register-input.component';
import { LoginInputComponent } from '../../items/login-input/login-input.component';
import {UsersService} from '../../services/backend-services';

@NgModule({
  declarations: [
    AuthComponent
  ],
  imports: [
      CommonModule,
      LoginInputModule,
      RegisterInputModule
  ],
  providers: [
    UsersService,
  ],
  entryComponents: [
      LoginInputComponent,
      RegisterInputComponent
  ],
})

export class AuthModule { }
