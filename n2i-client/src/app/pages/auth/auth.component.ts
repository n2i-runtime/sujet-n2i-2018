import { Component } from '@angular/core';

import { UsersService } from '../../services/backend-services';
import { User } from '../../services/backend-classes';

@Component({
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent {

  constructor(private _usersService: UsersService) { }

  public register(value) {
    this._usersService.add(value).subscribe(
      next => console.log(next),
      error => console.warn(error)
    );
  }
}
