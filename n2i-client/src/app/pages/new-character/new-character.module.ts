import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewCharacterComponent } from './new-character.component';
import {CharacterComponent} from "../../items/character/character.component";
import {CharacterModule} from "../../items/character/character.module";
import {ColorsPickerModule} from "../../items/colors-picker/colors-picker.module";
import {ColorsPickerComponent} from "../../items/colors-picker/colors-picker.component";

@NgModule({
  declarations: [
    NewCharacterComponent
  ],
  imports: [
      CommonModule,
      CharacterModule,
      ColorsPickerModule
  ],
  entryComponents: [
      NewCharacterComponent,
      CharacterComponent,
      ColorsPickerComponent
  ]
})

export class NewCharacterModule { }
