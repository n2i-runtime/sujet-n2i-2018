import { Component, OnInit } from '@angular/core';

import { UsersService } from '../../services/backend-services';

import { User } from '../../services/backend-classes';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  public usersList: User[];

  constructor(
    private _usersService: UsersService
  ) { }

  ngOnInit(): void {
    this.retrieveAllUsers();
  }

  private retrieveAllUsers() {
    this._usersService.getAll().subscribe(
      next => this.usersList = next,
      error => console.warn(error),
      () => {}
    );
  }
}
