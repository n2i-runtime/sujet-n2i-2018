import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersService } from '../../services/backend-services';

import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [
    UsersService,
  ],
  entryComponents: []
})

export class DashboardModule { }
