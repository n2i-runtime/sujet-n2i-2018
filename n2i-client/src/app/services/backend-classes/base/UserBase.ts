// This file should not be modified, as it can be overwritten by the generator.
// The 'User' class is here for customizations and will not be touched.


export class UserBase {
  public static readonly _resource: string = 'users';
  get _resource(): string { return UserBase._resource; };

  email: string;
  password: string;

}
