import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { User } from '../backend-classes';
// import { Observable } from 'rxjs';

@Injectable()
export class UsersService extends BackendService<User> {
  protected get resource() { return User._resource; }
  protected get class() { return User; }

}
