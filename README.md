# :sparkles: Projet N2I by Runtime team  :sparkles: #
 
## DÉSCRIPTION PROJET NUIT DE L'INFO ##

Vous êtes avec moi au cœur de ma future mission : 121 jours en solitaire 
dans  un  des  déserts  les  plus  chauds  de  la  planète  sur  les  sujets  du 
monitoring, de l’interopérabilité et de l'aide a
̀
 la décision.
Après   « Seul   dans   la   nuit   polaire »,   Stéphane   Lévin
(*)
   prépare   une 
nouvelle  exploration  scientifique  au  service  de  l’Homme.   La  Mission  « 
Seul  dans  le  Namib  »,  un  isolement  prolongé  en  totale  autonomie,  doit 
avoir  lieu  en  Namibie,  pendant  l’été  austral  (de  novembre  à  mars  de 
l’année suivante) avec une hygrométrie très faible.
Cette  expédition  pluridisciplinaire  se  veut  utile,  à  travers  trois  axes  de 
démonstration :
  1.   Valider   les   usages   et   les   bonnes   pratiques   pour   l’adaptation   et 
l’autonomie de l’humain dans les environnements extrêmes.
  2.  Qualifier  des  technologies  innovantes  et  des  nouveaux  usages  du 
monde connecté et prédictif dans les domaines de la santé, de l’habitat 
et de l’énergie, de la sécurité et des télécommunications, ...
   3.    Inventer    les    futurs    de    l’exploration    spatiale    en    préfigurant 
l'installation d'une base sur la lune et de nouveaux usages à bord de la 
station spatiale internationale. 

## TECHNOLOGIES UTILISEES ##

* Symfony4
* Angular 6
* MySQL 

## PRÉ-REQUIS ##

* Npm (v6.4.1)
* Composer

## NOS OBJECTIFS ##

* Permettre à l'utilisateur de se connecter par email/password
* Permettre à l'utilisateur de s'inscrire avec un email/password
* Permettre à l'utilisateur d'accéder à un tableau de bord, montrant l'état global de l'explorateur (fatigue, soif, faim...) à l'aide de la position gps pour savoir s'il est en activité ou non, en fonction, on augmente la soif/faim/fatigue ainsi que l'affichage de la météo
* Permettre à l'utilisateur de créer son personnage virtuel qui le représente dans la réalité (nom, prénom, âge, poids, taille...), ces informations nous aide aussi à déterminer son état de santé de base, sa corpulence (imc) etc...
* Permettre à l'utilisateur de créer des objectifs à atteindre via une map openStreetMap.
* Permettre à l'utilisateur de voir son inventaire, et de débuter une exploration en choisissant un objectif créé au préalable, selon l'objectif (distance à parcourir, météo, environnement), nous lui attribuant un ensemble d'outils/vivre à prendre avec lui.
* Permettre à l'utilisateur de checker l'ensemble de son matériel et de recenser son état, avec une liste permettant de se rappeler des commandes à effectuer pour changer l'équipement défectueux.

## AVANCEMENT ##

* Objectifs définis
* Structure du projet définie
* Système de communication entre le client angular et symfony4 avec api platform et lysis disponile (api)
* Page /authentification disponible
* Page /new-character disponible
* Database runtime et table user disponible (email, password)

Plus d'information sur : https://trello.com/b/toXEwHIB/nuit-de-linfo

## ÉTAPE 1 : INSTALLATION DES DEPENDANCES ##
  
  Pour installer les dépendances du projet il suffit de vous rendre dans deux dossiers.
  Le premier est le dossier n2i-api, en ligne de commande exécutez simplement :
  
  `npm install && composer install`
  
  Et pour le deuxième, faites de même en vous plaçant cette fois-ci dans le dossier client, n2i-client :
  
  `npm install && composer install`
  
## ÉTAPE 2 : LANCEMENT DU PROJET ##
  
  Le lancement s'effectue en deux étapes, la première est de faire fonctionner le serveur symfony, pour ce faire, placez-vous dans le dossier n2i-api du projet avec votre terminal et exécutez cette commande :
  
  `php bin/console server:run`
  
  La seconde étape consiste à lancer le client Angular, exécutez simplement cette commande en vous plaçant dans n2i-client :
  
  `ng serve`
  
  ## ÉTAPE 3 : Créer la base de données ##
  
  Vous devez avoir MySQL de lancé, avec les identifiants suivants :
  * host: localhost
  * user: root
  * password: toor
  
  ou bien modifier le dossier .env du dossier n2i-api et configurer la connexon selon vos informations de connexion à votre base de données locale ou distante
  
  Placez-vous avec votre terminal dans le dossier n2i-api et exécutez ces commandes :

  `php bin/console doctrine:database:create`
  
  `php bin/console doctrine:migrations:migrate`
  
## Avoir accès à la page de connexion ou inversement, se mettre comme utilisateur connecté ##
    
 Malheureusement, par manque de temps, l'interface de connexion n'est pas fonctionnelle côté serveur mais seulement client, pour pouvoir accéder à la page, il vous faudra
 mettre la valeur isAuthenticated à false dans le fichier n2i-client/src/app/guard/auth.guard.ts ligne 11 :

    private isAuthenticated = false;
    
 et de même avec le ficheir n2i-client/src/app/templates/authentification/authentification.component.ts ligne 10
    
    private isAuthenticated = false;
 
 A l'inverse, mettez true pour être concidéré comme utilisateur connecté.

## AUCUN FICHIER PERMETTANT D'AVOIR DES FONCTIONNALITEES EN PLUS N'A ETE MODIFIE APRES LE TEMPS ACCORDE A LA NUIT DE L'INFO, SEUL LE FICHIER README.MD ET LE PACKAGE.JSON ONT ETE MIS A JOUR !